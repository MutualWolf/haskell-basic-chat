module Main where

import System.Environment
import Data.Data

import Client
import Server
import Utils

main :: IO ()
main = do
  args <- getArgs
  let progType = parseArguments args
  if progType == Just Client
    then startClient
  else if progType == Just Server
    then startServer
  else
    putStrLn "Bad argument(s) : Use only one of -C for client or -S for server, or leave blank for client"