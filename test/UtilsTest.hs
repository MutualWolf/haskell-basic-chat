module UtilsTest where

import Test.HUnit
import Utils

givenNoArgsWhenParsingReturnsClient :: Test
givenNoArgsWhenParsingReturnsClient =
  TestCase $ assertEqual "Should return client when no argument is given"
    (Just Client) (parseArguments [])

givenDashCWhenParsingReturnsClient :: Test
givenDashCWhenParsingReturnsClient =
  TestCase $ assertEqual "Should return client when -C is given"
    (Just Client) (parseArguments ["-C"])

givenDashSWhenParsingReturnsServer :: Test
givenDashSWhenParsingReturnsServer =
  TestCase $ assertEqual "Should return server when -S is given"
    (Just Server) (parseArguments ["-S"])

givenInvalidArgWhenParsingReturnsNothing :: Test
givenInvalidArgWhenParsingReturnsNothing =
  TestCase $ assertEqual "Should return nothing when an invalid argument is given"
    Nothing (parseArguments ["f"])

givenMoreThanOneArgWhenParsingReturnsNothing :: Test
givenMoreThanOneArgWhenParsingReturnsNothing =
  TestCase $ assertEqual "Should return nothing when more than one argument is given"
    Nothing (parseArguments ["-C", "-S"])

getUtilsTestList :: Test
getUtilsTestList = TestList
  [TestLabel "givenNoArgs_whenParsing_returnsClient" givenNoArgsWhenParsingReturnsClient
  ,TestLabel "givenDashC_whenParsing_returnsClient" givenDashCWhenParsingReturnsClient
  ,TestLabel "givenDashS_whenParsing_returnsServer" givenDashSWhenParsingReturnsServer
  ,TestLabel "givenInvalidArg_whenParsing_returnsNothing" givenInvalidArgWhenParsingReturnsNothing
  ,TestLabel "givenMoreThanOneArg_whenParsing_returnsNothing" givenMoreThanOneArgWhenParsingReturnsNothing]