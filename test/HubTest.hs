module HubTest where

import Test.HUnit
import Hub
import qualified Data.ByteString.Char8 as B8

client1 = HubClient (B8.pack "Client1") Nothing Nothing Nothing
client2 = HubClient (B8.pack "Client2") Nothing Nothing Nothing
hubEmpty = []
hubClient1 = [client1]

givenHubAfterAddingClientSizeIncreases :: Test
givenHubAfterAddingClientSizeIncreases =
  TestCase $ assertEqual "Should increase size of hub when a client is added"
  2 (length $ addClient hubClient1 client2)

givenHubAfterAddingExistingClientSizeDoesNotIncrease :: Test
givenHubAfterAddingExistingClientSizeDoesNotIncrease =
  TestCase $ assertEqual "Should not increase size of hub when an existing client is added"
  1 (length $ addClient hubClient1 client1)

getHubTestList :: Test
getHubTestList = TestList
  [TestLabel "givenHub_afterAddingClient_sizeIncreases" givenHubAfterAddingClientSizeIncreases
  ,TestLabel "givenHub_afterAddingExistingClient_sizeDoesNotIncrease" givenHubAfterAddingExistingClientSizeDoesNotIncrease]