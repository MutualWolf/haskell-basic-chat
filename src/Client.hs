module Client where

import Network.Socket hiding (send, recv)
import Network.Socket.ByteString
import qualified Data.ByteString.Char8 as B8
import Control.Concurrent
import Control.Monad (unless, when)

startClient :: IO()
startClient = do
  addrInfo <- getAddrInfo Nothing (Just "127.0.0.1") (Just "4242")
  let serverAddr = head addrInfo
  sock <- socket (addrFamily serverAddr) Stream defaultProtocol
  connect sock (addrAddress serverAddr)
  forkIO (runReadFromSock sock)
  runWriteToSock sock
  close sock

runReadFromSock :: Socket -> IO ()
runReadFromSock sock = withSocketsDo $
  do
  msg <- recv sock 4096
  unless (B8.null msg) $ print msg
  runReadFromSock sock

runWriteToSock :: Socket -> IO ()
runWriteToSock sock = withSocketsDo $
  do
  msg <- B8.getLine
  unless (B8.null msg) $ sendToSock sock msg
  runWriteToSock sock

sendToSock :: Socket -> B8.ByteString -> IO ()
sendToSock sock msg = withSocketsDo $
  do
  send sock msg
  return ()