module Main where

import Test.HUnit
import ClientTest
import UtilsTest
import HubTest

main :: IO Counts
main = runTestTT $ TestList
    [TestLabel "Client" getClientTestList
    ,TestLabel "Utils" getUtilsTestList
    ,TestLabel "Hub" getHubTestList]