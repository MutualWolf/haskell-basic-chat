module Server where

import Network.Socket hiding (send, recv)
import Network.Socket.ByteString
import qualified Data.ByteString.Char8 as B8
import Control.Concurrent
import Control.Monad (unless)

startServer :: IO()
startServer = withSocketsDo $
  do
  sock <- socket AF_INET Stream 0
  setSocketOption sock ReuseAddr 1
  bind sock (SockAddrInet 4242 iNADDR_ANY)
  listen sock 10
  mainServerLoop sock

mainServerLoop :: Socket -> IO ()
mainServerLoop sock = withSocketsDo $
  do
  conn <- accept sock
  forkIO (runConn conn)
  mainServerLoop sock

runReadFromSock :: Socket -> IO ()
runReadFromSock sock = withSocketsDo $
  do
  msg <- recv sock 4096
  unless (B8.null msg) $ print msg
  runReadFromSock sock

runWriteToSock :: Socket -> IO()
runWriteToSock sock = withSocketsDo $
  do
  msg <- B8.getLine
  unless (B8.null msg) $ sendToSock sock msg
  runWriteToSock sock

sendToSock :: Socket -> B8.ByteString -> IO ()
sendToSock sock msg = withSocketsDo $
  do
  send sock msg
  return ()

runConn :: (Socket, SockAddr) -> IO ()
runConn (sock, _) = withSocketsDo $
  do
  forkIO (runReadFromSock sock)
  runWriteToSock sock
  close sock