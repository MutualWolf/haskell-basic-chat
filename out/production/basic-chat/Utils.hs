module Utils where

data ProgramType = Client | Server deriving (Eq, Show)

parseArguments :: [String] -> Maybe ProgramType
parseArguments []         = Just Client
parseArguments [arg]
  | arg == "-C" = Just Client
  | arg == "-S" = Just Server
  | otherwise   = Nothing
parseArguments _          = Nothing
