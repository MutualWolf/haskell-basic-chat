module Hub where

import Network.Socket
import qualified Data.ByteString.Char8 as B8
import Data.List

type Hub = [HubClient]

data HubClient = HubClient
  { userName :: B8.ByteString
  , addr :: Maybe SockAddr
  , sockWrite :: Maybe Socket
  , sockRead :: Maybe Socket }
instance Eq HubClient where
  (HubClient name1 addr1 _ _) == (HubClient name2 addr2 _ _ ) = (name1 == name2) && (addr1 == addr2)

addClient :: Hub -> HubClient -> Hub
addClient hub client = nub (hub ++ [client])

removeClient :: Hub -> HubClient -> Hub
removeClient hub client = filter (\c -> c /= client) hub